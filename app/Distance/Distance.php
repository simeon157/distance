<?php

namespace App\Distance;

/**
 * Class Distance
 * @package App\Distance
 */
class Distance
{

    // Store the value for a distance in meters to avoid complexity.
    private $value;

    /**
     * Distance constructor.
     * @param $value
     * @param $unit
     */
    public function __construct($value, $unit)
    {
        switch ($unit) {
            case 'meter':
                $this->value = $value;
                break;
            case 'yard':
                $this->value = $value / 1.0936;
                break;
        }
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }
}
