<?php

namespace App\Services;

use App\Distance\Distance;

/**
 * Class DistanceService
 * @package App\Services\Distance
 */

class DistanceCalculatorService
{
    private $sumDistance = 0;
    private $precision = 2;

    /**
     * Add a distance to the total
     * @param Distance $distance
     */
    public function addDistance(Distance $distance) {
        $this->sumDistance += $distance->getValue();
    }


    /**
     * Get the calculated sum in either meters or yards.
     *
     * @param $unit
     * @return array
     */
    public function getSumInUnit($unit) {
        return [
            'unit' => $unit,
            'value' => round($unit === 'meter' ? $this->sumDistance : $this->sumDistance * 1.0936133, $this->precision)
        ];
    }

    /**
     * Set the return precision.
     *
     * @param $precision
     */
    public function setPrecision($precision) {
        $this->precision = $precision;
    }

    /**
     * @return int
     */
    public function getPrecision()
    {
        return $this->precision;
    }

    /**
     * @param int $sumDistance
     */
    public function setSumDistance($sumDistance)
    {
        $this->sumDistance = $sumDistance;
    }

}
