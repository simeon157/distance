<?php

namespace App\Http\Controllers;

use App\Distance\Distance;
use App\Services\DistanceCalculatorService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;

class DistanceController extends Controller
{

    private $distanceCalculatorService;

    /**
     * Inject the DistanceCalculator dependency
     *
     * @param DistanceCalculatorService $distanceCalculatorService
     */
    public function __construct(DistanceCalculatorService $distanceCalculatorService)
    {
        $this->distanceCalculatorService = $distanceCalculatorService;
    }

    /**
     * Endpoint handler for distance endpoint.
     *
     * @param Request $request
     * @return Response
     */
    public function calculateDistance(Request $request): Response {

        $params = $request->all();

        $validator = Validator::make($params, [
            'distance' => 'required|array',
            'distance.*.value' => 'required|numeric',
            'distance.*.unit' => "required|string|in:meter,yard",
            'returnUnit' => "required|string|in:meter,yard",
            'precision' => "numeric"
        ]);

        if($validator->fails()) {
            return new Response($validator->errors());
        }

        // Optionally set precision
        if(isset($params['precision'])) {
            $this->distanceCalculatorService->setPrecision($params['precision']);
        }

        // Loop through each distance and add it to the total in the distance calculator.
        foreach ($params['distance'] as $distance) {
            $this->distanceCalculatorService->addDistance(new Distance($distance['value'], $distance['unit']));
        }

        return new Response($this->distanceCalculatorService->getSumInUnit($params['returnUnit']));
    }
}
