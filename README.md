
## About Project
This example project creates and API endpoint that takes as arguments a number of distances in either meters
or yards and returns their sum distance in either yards or meters.

The idea is to create an HTTP controller to handle the API GET request and return the correct data.

- Implemented a `DistanceController.php` to handle the API request
- Implemented a `Distance.php` class to represent a single distance. The class takes a value and an unit and internally converts each Distance to meters.
- Implemented a `DistanceCalculatorService.php` to handle calculating multiple `Distance objects` and return a sum value.
- Created a small Unit test to test the API.

## Setup
```
cd distances
```
```
composer install
```
```
php artisan serve
```

## Endpoint
Example GET request
```
http://localhost:8000/api/distance?distance[1][value]=5.5&distance[1][unit]=meter&distance[2][value]=3&distance[2][unit]=yard&returnUnit=meter&precision=2
```
Response
```
{
"unit": "meter",
"value": 8.24
}
```

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
