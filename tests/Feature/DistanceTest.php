<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DistanceTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testDistanceEndpoint()
    {
        $this->json('GET', 'api/distance', [
            "distance" => [
                1 => [
                    "value" => 5,
                    "unit" => "meter",
                ],
                2 => [
                    "value" => 3,
                    "unit" => "yard",
                ],
            ],
            "returnUnit" => "meter",
        ])->assertJson([
                "unit" => "meter",
                "value" => 7.74
        ]);
    }

    public function testDistanceEndpointPrecision() {
        $this->json('GET', 'api/distance', [
            "distance" => [
                1 => [
                    "value" => 5,
                    "unit" => "meter",
                ],
                2 => [
                    "value" => 3,
                    "unit" => "yard",
                ],
            ],
            "returnUnit" => "meter",
            "precision" => 5,
        ])->assertJson([
            "unit" => "meter",
            "value" => 7.74323
        ]);
    }

    public function testDistanceEndpointReturnUnit() {
        $this->json('GET', 'api/distance', [
            "distance" => [
                1 => [
                    "value" => 5,
                    "unit" => "meter",
                ],
                2 => [
                    "value" => 3,
                    "unit" => "yard",
                ],
            ],
            "returnUnit" => "yard",
        ])->assertJson([
            "unit" => "yard",
            "value" => 8.47,
        ]);
    }
}
